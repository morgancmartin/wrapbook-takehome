import { Dispatch, ChangeEventHandler, MouseEventHandler } from "react"
import { useMutation } from "blitz"
import { LabeledTextField } from "app/core/components/LabeledTextField"
import { Form, FORM_ERROR } from "app/core/components/Form"
import signup from "app/auth/mutations/signup"
import { Signup, password as Password, email as Email } from "app/auth/validations"
import { VStack, Text, Input, Button } from "@chakra-ui/react"
import { atom, useAtom, SetStateAction } from "jotai"

type SignupFormProps = {
  onSuccess?: () => void
}

const emailAtom = atom("")

const emailInvalidAtom = atom((get) => {
  const email = get(emailAtom)
  return !Email.safeParse(email).success
})

const passwordAtom = atom("")

const passwordInvalidAtom = atom((get) => {
  const password = get(passwordAtom)
  return !Password.safeParse(password).success
})

const buttonEnabledAtom = atom((get) => {
  const email = get(emailAtom)
  const password = get(passwordAtom)

  return email.length && password.length && Signup.safeParse({ email, password }).success
})

export const SignupForm = ({ onSuccess }: SignupFormProps) => {
  const [email, setEmail] = useAtom(emailAtom)
  const [password, setPassword] = useAtom(passwordAtom)
  const [emailInvalid] = useAtom(emailInvalidAtom)
  const [passwordInvalid] = useAtom(passwordInvalidAtom)
  const [buttonEnabled] = useAtom(buttonEnabledAtom)
  const [signupMutation] = useMutation(signup)
  const setEmailOnChange = setOnChange(setEmail)
  const setPasswordOnChange = setOnChange(setPassword)

  const submit: MouseEventHandler = async (e) => {
    const user = await signupMutation({
      email,
      password,
    })

    setEmail("")
    setPassword("")

    if (user.id && onSuccess) {
      onSuccess()
    }
  }

  const inputStyleProps = {
    display: "flex",
    alignItems: "center",
    padding: "12px 16px",
    w: "454px",
    h: "48px",
    background: "#F7F7FA",
    borderRadius: "12px",
  }

  return (
    <VStack
      w={["100%", "502px"]}
      bg="white"
      padding="24px"
      borderRadius="12px"
      boxShadow="0px 4px 24px rgba(0, 0, 0, 0.08)"
    >
      <Text color="black" fontWeight="bold" fontSize="22px" lineHeight="26px">
        Get started with Wrapbook
      </Text>
      <VStack w="100%" style={{ gap: "24px" }}>
        <Text fontSize="16px" lineHeight="150%" color="#555555">
          Tackle timecards, payroll, and insurance with Wrapbook, a powerful and easy to use
          platform for managing productions.
        </Text>
        <Input
          isRequired
          isInvalid={emailInvalid}
          onChange={setEmailOnChange}
          errorBorderColor="red.500"
          focusBorderColor={email.length && !emailInvalid ? "green.500" : undefined}
          borderColor={email.length && !emailInvalid ? "green.500" : undefined}
          type="email"
          placeholder="Enter your email address"
          maxW="100%"
          {...inputStyleProps}
        />
        {email.length && !emailInvalid && (
          <Input
            isRequired
            isInvalid={passwordInvalid}
            onChange={setPasswordOnChange}
            errorBorderColor="red.500"
            focusBorderColor={password.length && !passwordInvalid ? "green.500" : undefined}
            borderColor={password.length && !passwordInvalid ? "green.500" : undefined}
            type="password"
            placeholder="Enter a password"
            maxW="100%"
            {...inputStyleProps}
          />
        )}
        <Button
          onClick={submit}
          disabled={!buttonEnabled}
          display="flex"
          alignItems="center"
          padding="12px 16px"
          background={buttonEnabled ? "#0047FF" : "#A2A8B7"}
          borderRadius="12px"
          w={["100%", "454px"]}
          h="48px"
          color="white"
        >
          Continue to Wrapbook
        </Button>
      </VStack>
    </VStack>
  )
}

function setOnChange(
  setter: Dispatch<SetStateAction<string>>
): ChangeEventHandler<HTMLInputElement> {
  return (e) => {
    setter(e.target.value)
  }
}

export default SignupForm
