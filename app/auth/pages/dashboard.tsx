import { useRouter, BlitzPage, Routes, useQuery } from "blitz"
import Layout from "app/core/layouts/Layout"
import { Flex, Text } from "@chakra-ui/react"
import getPeople from "app/people/queries/getPeople"
import People from "app/core/components/People"
import InviteAside from "app/core/components/InviteAside"

const DashboardPage: BlitzPage = () => {
  const [people] = useQuery(getPeople, undefined)
  const router = useRouter()

  return (
    <Flex
      h="100%"
      w="100vw"
      overflowY={["hidden", "auto"]}
      overflowX="hidden"
      padding={["0", "48px 204px 0px"]}
      style={{ gap: "24px" }}
      justifyContent={["flex-start", "center"]}
      flexDir={["column", "row"]}
    >
      <Flex
        flexDir="column"
        w={["100%", "auto"]}
        h="100%"
        overflowY={["auto", "visible"]}
        overflowX={["hidden", "visible"]}
        padding={["0 16px", "0"]}
        marginBottom={["-24px", "0"]}
      >
        <Flex flexDir="column" marginBottom={["16px", "24px"]}>
          <Text color="black" fontWeight="bold" fontSize="22px" lineHeight="26px">
            Your company dashboard
          </Text>
          <Text fontSize="16px" lineHeight="150%" color="#555555" marginTop="8px">
            Manage people, view history & see how your projects are doing.
          </Text>
        </Flex>
        <People people={people} />
      </Flex>
      <InviteAside />
    </Flex>
  )
}

DashboardPage.getLayout = (page) => <Layout title="Sign Up">{page}</Layout>

export default DashboardPage
