import { useRouter, BlitzPage, Routes } from "blitz"
import Layout from "app/core/layouts/Layout"
import { SignupForm } from "app/auth/components/SignupForm"
import { Flex } from "@chakra-ui/react"

const SignupPage: BlitzPage = () => {
  const router = useRouter()

  return (
    <Flex h="100%" w="100vw" justifyContent="center" alignItems="center" padding={["0 16px", "0"]}>
      <SignupForm onSuccess={() => router.push(Routes.DashboardPage())} />
    </Flex>
  )
}

SignupPage.redirectAuthenticatedTo = "/dashboard"
SignupPage.getLayout = (page) => <Layout title="Sign Up">{page}</Layout>

export default SignupPage
