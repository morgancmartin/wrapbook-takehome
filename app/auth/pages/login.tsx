import { useRouter, BlitzPage } from "blitz"
import Layout from "app/core/layouts/Layout"
import { LoginForm } from "app/auth/components/LoginForm"
import { Flex } from "@chakra-ui/react"

const LoginPage: BlitzPage = () => {
  const router = useRouter()

  return (
    <Flex h="100%" w="100vw" justifyContent="center" alignItems="center" padding={["0 16px", "0"]}>
      <LoginForm
        onSuccess={() => {
          router.push("/dashboard")
        }}
      />
    </Flex>
  )
}

LoginPage.redirectAuthenticatedTo = "/dashboard"
LoginPage.getLayout = (page) => <Layout title="Log In">{page}</Layout>

export default LoginPage
