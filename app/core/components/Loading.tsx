import { HStack, Spinner } from "@chakra-ui/react"

export default function Loading() {
  return (
    <HStack w="100vw" h="100vh" alignItems="center" justifyContent="center">
      <Spinner color="blue.500" />
    </HStack>
  )
}
