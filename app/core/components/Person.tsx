import { Image } from "blitz"
import { Flex, Text, Box, Icon } from "@chakra-ui/react"
import { BsChevronRight, BsClipboard, BsCoin } from "react-icons/bs"
import { BiTimeFive } from "react-icons/bi"
import { AiFillCheckCircle } from "react-icons/ai"

export interface Props {
  email: string
  id: number
  name: string
}

export default function Person({ email, id, name }: Props) {
  const avatarNum = { 0: "one", 1: "two", 2: "three" }[id % 3]

  return (
    <Flex
      w={["100%"]}
      background="rgba(255, 255, 255, 0.94)"
      borderRadius="12px"
      flexDir="column"
      alignItems="flex-start"
      padding="16px"
      h={["126px", "auto"]}
    >
      <Flex h={["auto", "222px"]} w={["100%"]} flexDir="column">
        <Flex alignItems="center" style={{ gap: "24px" }}>
          <Flex flexBasis={["48px", "64px"]} h={["48px", "64px"]} minW={["48px", "64px"]}>
            <Image src={`/avatars/image-${avatarNum}.png`} width="64px" height="64px" />
          </Flex>
          <Flex flex="1" flexDir="column" marginTop={["0", "12px"]} overflowX="hidden">
            <Text color="black" fontWeight="bold" fontSize="17px" lineHeight="20px">
              {name}
            </Text>
            <Text
              color="#979797"
              fontSize="14px"
              lineHeight="16px"
              maxH="32px"
              textOverflow="ellipsis"
              overflow="hidden"
            >
              Employee #{id} • {email}
            </Text>
          </Flex>
          <Flex w="fit-content" justifyContent="flex-end">
            <Icon as={BsChevronRight} cursor="pointer" />
          </Flex>
        </Flex>
        <Divider />
        <Flex display={["none", "flex"]} h={["62px"]} alignItems="center" style={{ gap: "8px" }}>
          <Flex
            padding="6px"
            alignItems="center"
            h={["30px"]}
            justifyContent="space-between"
            border="1.5px solid rgba(0, 0, 0, 0.04)"
            borderRadius="8px"
          >
            <Icon as={BiTimeFive} cursor="pointer" marginRight="6px" />
            <Text color="black" fontSize="13px" lineHeight="16px">
              Pending: $7.00
            </Text>
          </Flex>
          <Flex
            padding="6px"
            alignItems="center"
            h={["30px"]}
            justifyContent="space-between"
            border="1.5px solid rgba(0, 0, 0, 0.04)"
            borderRadius="8px"
          >
            <Icon as={BsClipboard} cursor="pointer" marginRight="6px" />
            <Text color="black" fontSize="13px" lineHeight="16px">
              Approved: $62.12
            </Text>
          </Flex>
          <Flex
            padding="6px"
            alignItems="center"
            h={["30px"]}
            justifyContent="space-between"
            border="1.5px solid rgba(0, 0, 0, 0.04)"
            borderRadius="8px"
          >
            <Icon as={BsCoin} cursor="pointer" marginRight="6px" />
            <Text color="black" fontSize="13px" lineHeight="16px">
              Paid: $0.00
            </Text>
          </Flex>
        </Flex>
        <Divider />
        <Flex
          alignItems="center"
          h={["32px"]}
          padding="6px"
          marginTop={["16px", "0"]}
          style={{ gap: "8px" }}
        >
          <Flex background="rgba(255, 255, 255, 0.94)" borderRadius="8px" alignItems="center">
            <Icon
              h={["20px"]}
              w={["20px"]}
              as={AiFillCheckCircle}
              color="#27AE60"
              marginRight="6px"
            />
            <Text color="black" fontSize="13px" lineHeight="16px">
              Account created
            </Text>
          </Flex>
          <Flex background="rgba(255, 255, 255, 0.94)" borderRadius="8px" alignItems="center">
            <Icon
              h={["20px"]}
              w={["20px"]}
              as={AiFillCheckCircle}
              color="#27AE60"
              marginRight="6px"
            />
            <Text color="black" fontSize="13px" lineHeight="16px">
              Onboarded
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  )
}

function Divider() {
  return (
    <Flex
      display={["none", "flex"]}
      background="rgba(0, 0, 0, 0.08)"
      borderRadius="16px"
      margin="16px 0px"
      w={["100%"]}
      h={["1px"]}
    />
  )
}
