import { MouseEventHandler } from "react"
import { Link as PageLink, Routes, useMutation, useRouter } from "blitz"
import {
  HStack,
  VStack,
  Link,
  Button,
  Flex,
  Text,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Box,
} from "@chakra-ui/react"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import logoutResolver from "app/auth/mutations/logout"

export default function Header() {
  return (
    <HStack w="100%" h={["48px", "64px"]}>
      <Flex w="50%" justifyContent="flex-start">
        <Text
          fontWeight="bold"
          fontSize="18px"
          lineHeight="21px"
          color="black"
          marginLeft={["16px", "204px"]}
        >
          Wrapbook
        </Text>
      </Flex>
      <Flex w="50%" justifyContent="flex-end">
        <HeaderButtons />
      </Flex>
    </HStack>
  )
}

function HeaderButtons() {
  const currentUser = useCurrentUser()
  const [logoutMutation] = useMutation(logoutResolver)
  const router = useRouter()

  const logout: MouseEventHandler<HTMLAnchorElement> = async (e) => {
    await logoutMutation()
    router.push("/login")
  }

  const LinkStyleProps = {
    fontWeight: "500",
    fontSize: "16px",
    lineHeight: "19px",
    color: "black",
  }

  return (
    <>
      <HStack
        w="100%"
        h={["auto", "50px"]}
        align="center"
        justifyContent="flex-end"
        marginRight="204px"
        style={{ gap: "16px" }}
        display={["none", "flex"]}
      >
        <Link {...LinkStyleProps}>Link</Link>
        <Link {...LinkStyleProps}>Link</Link>
        {currentUser ? (
          <></>
        ) : (
          <PageLink href={Routes.LoginPage()}>
            <Link {...LinkStyleProps}>Login</Link>
          </PageLink>
        )}
        {!currentUser ? (
          <PageLink href={Routes.SignupPage()}>
            <Button
              {...LinkStyleProps}
              bgColor="white"
              boxShadow="0px 4px 16px rgba(0, 0, 0, 0.08)"
            >
              Signup
            </Button>
          </PageLink>
        ) : (
          <Link {...LinkStyleProps} onClick={logout}>
            Logout
          </Link>
        )}
      </HStack>
      <Box display={["block", "none"]} marginRight="16px">
        <Menu>
          <MenuButton as={Text} fontSize="17px" lineHeight="125%">
            Menu
          </MenuButton>
          <MenuList>
            <MenuItem>
              <Link>Link</Link>
            </MenuItem>
            <MenuItem>
              <Link>Link</Link>
            </MenuItem>
            {currentUser ? (
              <></>
            ) : (
              <MenuItem>
                <PageLink href={Routes.LoginPage()}>
                  <Link>Login</Link>
                </PageLink>
              </MenuItem>
            )}
            {!currentUser ? (
              <MenuItem>
                <PageLink href={Routes.SignupPage()}>
                  <Link>Signup</Link>
                </PageLink>
              </MenuItem>
            ) : (
              <MenuItem>
                <Link onClick={logout}>Logout</Link>
              </MenuItem>
            )}
          </MenuList>
        </Menu>
      </Box>
    </>
  )
}
