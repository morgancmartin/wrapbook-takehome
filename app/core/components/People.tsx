import { VStack } from "@chakra-ui/react"
import Person, { Props as PersonProps } from "app/core/components/Person"

interface Props {
  people: PersonProps[]
}

export default function People({ people }: Props) {
  console.log(people)
  return (
    <VStack w={["100%", "680px"]} paddingBottom={["16px", "64px"]} style={{ gap: "24px" }}>
      {people.map((person) => (
        <Person {...person} key={person.id} />
      ))}
    </VStack>
  )
}
