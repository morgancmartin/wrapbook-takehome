import { useState, useEffect } from "react"
import { Image } from "blitz"
import InvitePic from "public/invite-pic.png"
import { Text, Flex, Button, Icon } from "@chakra-ui/react"
import { atom, useAtom } from "jotai"
import { AiFillCheckCircle } from "react-icons/ai"

const urlAtom = atom("https://wrapbook.com/i/j93kda92")

interface Props {}

export default function InviteAside(props: Props) {
  const [url] = useAtom(urlAtom)
  const [copied, setCopied] = useState(false)
  const copyToClipboard: React.MouseEventHandler = (e) => {
    navigator.clipboard.writeText(url)
    setCopied(true)
  }

  useEffect(() => {
    if (copied) {
      setTimeout(() => setCopied(false), 1000)
    }
  }, [copied])

  return (
    <>
      <Flex
        display={["none", "flex"]}
        w="328px"
        h="380px"
        padding="24px"
        background="#FFF7EE"
        borderRadius="12px"
        flexDir="column"
        alignItems="center"
        marginTop={["0", "60px"]}
      >
        <Flex h="96px" w="96px" borderRadius="100%" bgColor="white">
          <Image src={InvitePic} />
        </Flex>
        <Text
          color="black"
          fontWeight="bold"
          fontSize="17px"
          lineHeight="20px"
          textAlign="center"
          marginTop="24px"
        >
          Invite people to this project
        </Text>
        <Text fontSize="15px" lineHeight="18px" textAlign="center" color="#979797" marginTop="16px">
          Share the following link with your team to have them sign up and send you their invoices.
        </Text>
        <Flex bgColor="white" w="228px" h="34px" borderRadius="12px" padding="8px" marginTop="24px">
          <Text
            color="#033FFF"
            fontSize="15px"
            lineHeight="18px"
            letterSpacing="-0.003em"
            textDecoration="underline"
            cursor="pointer"
          >
            {url}
          </Text>
        </Flex>
        <Button
          onClick={copyToClipboard}
          h="40px"
          w="280px"
          color="#033FFF"
          fontWeight="bold"
          fontSize="14px"
          lineHeight="17px"
          marginTop="24px"
          bgColor="white"
          border="1.5px solid rgba(0, 0, 0, 0.08)"
          borderRadius="12px"
        >
          {copied ? <Icon as={AiFillCheckCircle} h="20px" w="20px" /> : "Copy to clipboard"}
        </Button>
      </Flex>
      <Button
        display={["flex", "none"]}
        onClick={copyToClipboard}
        h="48px"
        w="100%"
        color="#0047FF"
        fontWeight="bold"
        fontSize="15px"
        lineHeight="18px"
        bgColor="white"
      >
        {copied ? (
          <>
            <Icon as={AiFillCheckCircle} h="20px" w="20px" marginRight="10px" /> Invite link copied
            to your clipboard
          </>
        ) : (
          "Invite someone to Wrapbook"
        )}
      </Button>
    </>
  )
}
