import { Head, BlitzLayout } from "blitz"
import { VStack } from "@chakra-ui/react"
import Header from "app/core/components/Header"

const Layout: BlitzLayout<{ title?: string }> = ({ title, children }) => {
  return (
    <>
      <Head>
        <title>{title || "wrapbook-takehome"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <VStack h="100vh" w="100vw" background="#f2f6fb">
        <Header />
        {children}
      </VStack>
    </>
  )
}

export default Layout
