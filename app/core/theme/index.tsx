import { extendTheme } from "@chakra-ui/react"

const theme = extendTheme({
  fonts: {
    body: "SF Pro Display",
  },
})

export default theme
