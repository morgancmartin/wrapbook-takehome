export default async function getPeople() {
  const response = await fetch("https://castandcrew.herokuapp.com/people", { method: "GET" })
  const result = await response.json()

  return result?.collection
}
