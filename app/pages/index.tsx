import { useEffect } from "react"
import { Suspense } from "react"
import { Image, Link, BlitzPage, useMutation, Routes, useRouter, useSession } from "blitz"
import Layout from "app/core/layouts/Layout"
import { useCurrentUser } from "app/core/hooks/useCurrentUser"
import logout from "app/auth/mutations/logout"
import logo from "public/logo.png"

const Home: BlitzPage = () => {
  const router = useRouter()
  const session = useSession()

  useEffect(() => {
    router.push("/signup")
  })

  return <div />
}

Home.redirectAuthenticatedTo = "/dashboard"
Home.getLayout = (page) => <Layout title="Home">{page}</Layout>

export default Home
